# Knight HTML framework

version 1.0.2

このモジュールは HTML、CSS、Javascript を自動的に生成するためのフレームワークです。

例えば、ヘッダー、フッターが同じ HTML の大量生産や、複雑になりがちな CSS ファイルや Javascript ファイルの構造を簡易化を目指しています。

HTML には [EJS](http://ejs.co/)、CSS には [Sass](http://sass-lang.com/)、Javascript には [Babel(ES6)](https://babeljs.io/learn-es2015/) を使用します。
使っているライブラリ、モジュールについては下記にて詳しく説明します。

## お願い
このフレームワークはターミナルやコマンドプロンプトでインストール、各ファイルのコンパイル、サーバーの設置などの操作を行います。

このドキュメント内で「コマンドを実行」と書かれていたらターミナル、もしくはコマンドプロンプトで指定のコマンドを実行してください。

なるべく複雑なコマンドは使用しないようにしました。覚えるコマンドは片手で余るほどです。

すべて、このドキュメントに記述してありますので、忘れたらこちらをご確認ください。

## インストール方法
[Node.js](https://nodejs.org/ja/) の npm を使用してパッケージ管理を行っております。
まず、[ここ](https://nodejs.org/ja/)から [Node.js](https://nodejs.org/ja/) をインストールしてください。

インストールが終わったら、このプロジェクトのディレクトリに移動して、下記のコマンドを実行します。

```
npm install
```

## 各フォルダについて

### dist
自動生成した HTML、CSS、Javascript が入るディレクトリです。

ビルドは**中にある全てのファイルを削除して**からコンパイルを行います。

必要なファイルはすべて下記に記述する src ディレクトリに入れてください。

### src
生成に使用するための EJS、Sass、Javascript のソースコードやイメージファイルを置きます。
コンパイルされると其々の下記のディレクトリに出力されます。

- src/img/\*.\* -> dist/img/\*.\*
- src/css/\*.scss -> dist/css/\*.css
- src/js/\*.js -> dist/js/\*.js
- src/\*.ejs -> dist/\*.html

`_` で始まるファイルは、ejs、scss、js の全てのファイルでコンパイルから除外されます。

つまり、index.ejs は index.html になりますが、_header.ejs は header.html にはなりません。

部品ファイルとして使用してください。

### build
このフレームワークに必要なツールが入っています。
通常は変更など必要ありません。

### config.js
すべての ejs ファイルで使用できる変数を指定しています。

必要に応じて編集してください。

## 作業環境の作り方
インストールが終わったら下記のコマンドを実行します。

```
npm run dev
```

すると自動でインデックスページ（/dist/index.html）が PC に指定してあるデフォルトのブラウザで開きます。

`Ctrl-c` で実行中の作業環境を終了させることができます。

[SassLint](https://www.npmjs.com/package/sass-lint) と [ESLint](http://eslint.org/) を使用しています。

Lint とはプロジェクトで指定した文法の間違いを指摘してくれるツールです。

複数の人が携わるプロジェクトでは文法の不統一が混乱を招くこともあります。

Lint で指摘された間違いは修正していきましょう。

間違いがある状態でデプロイすることは避けるようにしましょう。

## ビルドの仕方
リリース用のファイルは下記のコマンドで生成します。

**dist 配下のファイルすべてが削除される**ので注意してください。

```
npm run build
```

## デプロイの仕方
ビルドしたファイルをFTP 経由でアップロードすることがコマンドからできます。

`./.ftp-config.js` に書かれてある接続先を使用します。
`./.ftp-config.js` の中身は必要な情報に書き換えてください。

production にデプロイ：

```
npm run deploy
```

develop にデプロイ：

```
npm run deploy -- develop
```

test にデプロイ（設定が書かれていた場合）：

```
npm run deploy -- test
```

設定を増やすだけデプロイ先を増やせるようになります。

デプロイする時はビルドしたものが正常に動いていることを確認してしましょう。

また Lint のエラーが発生していないことも確認しましょう。

## ドキュメントファイル
このフレームワークでは [Sassdoc](http://sassdoc.com/) と [JSDoc](http://usejsdoc.org/) を利用し Sass と Javascript のドキュメントを自動作成出来るようにしています。

下記のコマンドを実行すると docs 配下に sassdoc と jsdoc が生成されます。

```
npm run docs
```

- [Sassdoc](http://sassdoc.com/) とは、\*.sass 、もしくは \*.scss に書いたコメントを、そのままドキュメントにできるようにしたビルドシステムです。
- [JSDOC](http://usejsdoc.org/) とは、\*.js に書いたコメントを、そのままドキュメントにできるようにしたビルドシステムです。

ドキュメントを残すのは次の開発者のためでなく、あなたのためになります。

## 開発の仕方
### HTML
HTML には [EJS](http://ejs.co/) が使用できます。

[EJS](http://ejs.co/) とは HTML 内で Javascript が使用できる HTML テンプレートです。

例えば、下記のような記述ができます。

```
<% if (user) { %>
  <h2><%= user.name %></h2>
<% } %>
```

結果（user = { name: 'マル' }）:

```
  <h2>マル</h2>
```

作った ejs を埋め込むことも可能です。

_header.ejs

```
<html>
<head>
<title><%= title ? title : 'Knight' %></title>
</head>
<body>
  <div id="wrap">
```

_footer.ejs

```
  </div>
</body>
</html>
```

index.ejs

```
<%- include('_header.ejs', { title: '' }) %>
<div>
  コンテンツ
</div>
<%- include('_footer.ejs') %>
```

結果：

```
<html>
<head>
<title>Knight</title>
</head>
<body>
  <div id="wrap">
    <div>
      コンテンツ
    </div>
  </div>
</body>
</html>
```

src の配下に作った ejs ファイルが HTML に変換されます。

例外として `_` で始まるファイルは除外されます。

詳しい記述方法は [EJS](http://ejs.co/) のサイトを参考にしてください。

[こちら](http://qiita.com/y_hokkey/items/31f1daa6cecb5f4ea4c9)のサイトも参考になります。

## CSS
CSS には Sass が利用できます。

src/sass の配下に作った sass ファイルが css に変換されます。

例外として `_` で始まるファイルは除外されます。

#### コーディング規約
[BEM](https://github.com/juno/BEM-methodology-ja) の概念を取り入れます。

BEM は[こちら](http://qiita.com/pugiemonn/items/964203782e1fcb3d02c3) で優しく説明してありますので参考にしてください。

しかし、上記の BEM の方式では問題点があるので、いくつかの例外を設けます。

- 単語はキャメルケースで表します。
- Element の区切りは `__` とします。
- Modifier は、Block、Element と連続して書かないこととします。
- Modifier は、`-` 始まりとします。
- Modifier のキーと値の区切りは `_` とします。
- Modifier のキーは任意とします。

例

```
<nav class="gnav"><!-- Block -->
  <ul class="gnavManu"><!-- Block -->
    <li class="gnavManu__item -shop">ショップ</li><!-- Element + Modifier -->
    <li class="gnavManu__item -galary">ギャラリー</li><!-- Element + Modifier -->
    <li class="gnavManu__item -mypage -act">マイページ</li><!-- Element + Modifier + Modifier -->
  </ul>
</nav>

<div class="submitBox"><!-- Block -->

  <!-- Element // デフォルトのボタン -->
  <input class="submitBox__btn" type="button" value="戻る" />

  <!-- Element + Modifier + Modifier // 送信用のボタン + 色は青 -->
  <input class="submitBox__btn -submit -color_blue" type="button" value="送信" />

  <!-- Element + Modifier + Modifier // キャンセルのボタン + 色は赤 -->
  <input class="submitBox__btn -cancel -color_red" type="button" value="キャンセル" /><!-- Element + Modifier + Modifier -->

  <!-- Element + Modifier // デフォルトボタン + 色はオレンジ -->
  <input class="submitBox__btn -color_orange" type="button" value="確認" />
</div>
```

## Javascript
[Babel(ES6)](https://babeljs.io/learn-es2015/) が利用できます。

従来の Javascript（ES5）ではたりなかった機能が追加されています。

例えば、別のファイルをインクルードすることができます。

```
import from './_pc.js'
import from './_sp.js'
import Util from './util/_index.js'

let counter = Util.count(1, 5)
```

もちろん、無理をして使用する必要はありません。

さらに、[Browserify](http://browserify.org/) を包容しているので Node.js の外部モジュールを取り入れることもできます。

```
// jQuery
import $ from 'jquery'
// Vue
import Vue from 'Vue'

$(() => {
  // script
})
```

こちらも無理して使う必要はありません。

#### コーディング規約
[Node.js の規約](http://popkirby.github.io/contents/nodeguide/style.html) に準じます。

# 参考サイト
- [EJS: HTMLテンプレート - http://ejs.co/](http://ejs.co/)
- [SASS: CSSスクリプト言語 - http://sass-lang.com/](http://sass-lang.com/)
- [Node.js: Javascript実行環境 - https://nodejs.org/ja/](https://nodejs.org/ja/)
- [Babel(ES6): 新しく勧告された Javascript の記述方法 - https://babeljs.io/learn-es2015/](https://babeljs.io/learn-es2015/)
- [Browserify: 外部モジュールを使うためのツール - http://browserify.org/](http://browserify.org/)
- [Browser Sync: ライブリロード HTML サーバー - https://browsersync.io/](https://browsersync.io/)
- [BEM: CSS アーキテクチャ - https://github.com/juno/BEM-methodology-ja](https://github.com/juno/BEM-methodology-ja)

- [Qiita - テンプレートエンジンEJSで使える便利な構文まとめ](http://qiita.com/y_hokkey/items/31f1daa6cecb5f4ea4c9)
- [Qiita - こんなHTMLとCSSのコーディング規約で書きたい](http://qiita.com/pugiemonn/items/964203782e1fcb3d02c3)

# 機能の拡張・修正が必要な場合

k.naito@coosy.co.jp にお問い合せください。

こんなことに対応します。

- タブではなくスペースのインデントにしたい。
- scss ではなく sass で書きたい。
- img フォルダ ではなくて assets フォルダにしたい。
- src 配下で使えるフォルダを増やしたい。
- lint の設定を変えたい。
- FTP の接続先の設定ができない・・・。
- 作業環境と本番環境で使用する変数を変えたい。（ejs）
- とにかく使い方わからないよー！！教えて！

(｀・ω・´) < ナイトが頑張って対応するよー。
