import path from 'path'
import version from './build/version'

export const branch = {
  host:'exsample.co.jp',
  user:'knight',
  pass:'cool',
  remotePath: '/public_html/' + version.GIT_BRANCH,
  src: 'dist/**',
  base: 'dist'
}

export const develop = {
  host:'exsample.co.jp',
  user:'knight',
  pass:'cool',
  remotePath: '/public_html/',
  src: 'dist/**',
  base: 'dist'
}

export const production = {

}
