/**
 * Gulp config
 */
import path from 'path'

export const ROOT_DIR = path.join(__dirname, '../')
export const DIST_DIR = path.join(ROOT_DIR, './dist')
export const SRC_DIR = path.join(ROOT_DIR, './src')

export default {
  ejs: {
    exp: 'html',
    data: './config',
    src: [
      './src/!(_)*.ejs',
      './src/**/!(_)*.ejs'
    ],
    watch: [
      './src/*.ejs',
      './src/**/*.ejs',
      './config.js'
    ],
    beautify: {
      "preserve_newlines": false,
      "indent_with_tabs": true
    }
  },
  sass: {
    src: [
      './src/css/*.scss',
      './src/css/**/*.scss'
    ],
    options: {
      indentType: 'tab',
      indentWidth: 1
    }
  },
  ecmascript: {
    src: [
      './src/js/!(_)*.js',
      './src/js/**/!(_)*.js'
    ],
    watch: [
      './src/js/*.js',
      './src/js/**/*.js'
    ]
  },
  assets: {
    src: [
      './src/img/*.*',
      './src/img/**.*',
    ]
  },
  sassdoc: {
    dest: "docs/sassdoc"
  },
  jsdoc: {
    "tags": {
      "allowUnknownTags": true,
      "dictionaries": ["jsdoc","closure"]
    },
    "plugins": ["plugins/markdown"],
    "templates": {
      "cleverLinks": false,
      "monospaceLinks": false
    },
    "opts": {
      "encoding": "utf8",
      "destination": "./docs/jsdoc",
      "recurse": true
    }
  }
}
