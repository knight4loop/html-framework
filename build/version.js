import moment from 'moment'
import childProcess from 'child_process'
import { name, version } from '../package.json'

export default {
  NAME: name,
  VERSION: version,
  GIT_BRANCH: exec('git rev-parse --abbrev-ref HEAD'),
  GIT_REVISION: exec('git rev-parse HEAD').slice(0, 10),
  BUILD_ON: moment().format('YYYY-MM-DDTH:mm:ssZ')
}

function exec (cmd) {
  let res = ''
  try {
    res =  childProcess.execSync(cmd).toString().replace(/[\n\r]/, '')
  } catch (e) {
  }
  return res
}
