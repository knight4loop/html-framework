import gulp from 'gulp'
import del from 'del'
import path from 'path'
import { ROOT_DIR, DIST_DIR } from '../.gulp-config'

let $ = require('gulp-load-plugins')();

let src = path.join(DIST_DIR, './**')

gulp.task('delete', (cb) => {
  return del(src, cb).then(files => {
    files.forEach(file =>
      $.util.log('[Delete] ' + path.relative(ROOT_DIR, file))
    )
  })
});
