import gulp from 'gulp'
import path from 'path'
import ejs from 'ejs'
import extend from 'extend'
import debug from './lib/debug'
import version from '../version'
import config, { ROOT_DIR, SRC_DIR, DIST_DIR } from '../.gulp-config'

let $ = require('gulp-load-plugins')()

gulp.task('compile:ejs', () =>
  gulp.src(config.ejs.src, { base: SRC_DIR, cwd: ROOT_DIR })
    .pipe(debug())
    .pipe($.plumber({
      errorHandler: $.notify.onError(error => {
        return {
          title: error.name,
          message: error.message
        }
      })
    }))
    .pipe(htmlify())
    .pipe($.htmlBeautify(config.ejs.beautify))
    .pipe(gulp.dest(DIST_DIR))
    .pipe(debug())
);

gulp.task('watch:ejs', ['compile:ejs'], () =>
  $.watch(config.ejs.watch, { cwd: ROOT_DIR }, () => gulp.start('compile:ejs'))
);

function htmlify () {
  let data = config.ejs.data;

  if (typeof data === 'string') {
    let dataPath = path.join(ROOT_DIR, config.ejs.data);

    let hash = require.resolve(dataPath);
    let cache = require.cache[hash];

    delete require.cache[hash];

    data = require(dataPath);

    if (cache && JSON.stringify(data) !== JSON.stringify(cache.exports)) {
      ejs.clearCache();
    }
  }

  let ext = config.ejs.ext || '.html';
  if (ext.indexOf('.') !== 0){
    ext = '.' + ext;
  }

  return $.ejs(extend({ version }, data), { ext });
}
