import gulp from 'gulp'
import path from 'path'
import browserify from 'browserify'
import watchify from 'watchify'
import babelify from 'babelify'
import through2 from 'through2'
import minimist from 'minimist'
import pool from './lib/pool'
import debug from './lib/debug'
import config, { SRC_DIR, DIST_DIR, ROOT_DIR } from '../.gulp-config'

let $ = require('gulp-load-plugins')()

let argv = minimist(process.argv.slice(2))

const IS_PRODUCTION = argv.for === true || argv.for === 'production'

gulp.task('compile:ecmascript', () => compile());

gulp.task('watch:ecmascript', ['compile:ecmascript'], () =>
  $.watch(config.ecmascript.watch, { cwd: ROOT_DIR }, () => compile({ watch: true }))
);

gulp.task('docs:ecmascript', cb => {
  gulp.src(config.ecmascript.src, { read: false, base: SRC_DIR, cwd: ROOT_DIR })
    .pipe($.jsdoc3(config.jsdoc, cb))
});

function compile (options = {}) {
  return gulp.src(config.ecmascript.src, { base: SRC_DIR, cwd: ROOT_DIR })
    .pipe(debug())
    .pipe($.plumber({
      errorHandler: $.notify.onError(error => {
        return {
          title: error.name,
          message: error.message
        }
      })
    }))
    .pipe($.eslint(path.join(ROOT_DIR, '.eslintrc')))
    .pipe($.eslint.result((result) => {
      pool('eslint', result)
    }))
    .pipe($.eslint.format())
    .pipe($.eslint.failOnError())
    .pipe(browserified(options))
    .pipe($.sourcemaps.init({ loadMaps: true }))
    .pipe($.if(IS_PRODUCTION, $.uglify()))
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest(DIST_DIR))
    .pipe(debug())
}

// 複数ファイルで browserify を使う方法
// http://qiita.com/ques0942/items/ad9428661e385fddb257
// 基本的な watchify の使い方
// http://yami-beta.hateblo.jp/entry/2016/09/12/175714
// browserify + watchify + babelify で遅くならない方法。
// http://akabeko.me/blog/2015/02/watchify/
let bundlers = {}
function browserified (options = {}) {
  return through2.obj((file, encode, callback) => {
    let { path } = file
    let { watch } = options
    let bundler = bundlers[path]

    if (!bundler) {
      let setting = {
        plugin: [],
        transform: [babelify]
      }
      if (watch) {
        setting.cache = {}
        setting.packageCache = {}
        setting.plugin.push(watchify)
      }
      bundler = bundlers[path] = browserify(path, setting)
    }

    return bundler
      .bundle((err, res) => {
        if (err) {
          $.util.log(err.stack)
          file.contents = null
          pool('browserify', {
            filename: file.path,
            message: err.stack
          })
        } else {
          $.util.log(`compiled : ${file.relative}`)
          file.contents = res
          pool('browserify', { filename: file.path })
        }
        callback(null, file)
      })
  })
};
