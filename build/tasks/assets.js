import gulp from 'gulp'
import path from 'path'
import debug from './lib/debug'
import config, { SRC_DIR, DIST_DIR, ROOT_DIR } from '../.gulp-config'

let $ = require('gulp-load-plugins')();

gulp.task('compile:assets', () =>
  gulp.src(config.assets.src, { base: SRC_DIR, cwd: ROOT_DIR })
    .pipe(debug())
    .pipe($.plumber())
    .pipe($.cached('assets'))
    .pipe(gulp.dest(DIST_DIR))
    .pipe(debug())
);

gulp.task('watch:assets', ['compile:assets'], () =>
  $.watch(config.assets.src, { cwd: ROOT_DIR }, () => gulp.start('compile:assets'))
);
