import gulp from 'gulp'
import stripAnsi from 'strip-ansi'
import BrowserSync from 'browser-sync'
import config from '../.bs-config'

const bs = BrowserSync.create()

gulp.task('browser-sync', () => bs.init(config));

export function onClientConnected (fnc) {
  bs.emitter.on('client:connected', fnc)
}

export function logger (message, type = 'info') {
  bs.sockets.emit('bs-msg:' + type, stripAnsi(message))
}
