import gulp from 'gulp'
import path from 'path'
import minimist from 'minimist'
import ftp from 'vinyl-ftp'
import extend from 'extend'
import * as ftpConfig from '../../.ftp-config'
import { ROOT_DIR } from '../.gulp-config'

let $ = require('gulp-load-plugins')();

let argv = minimist(process.argv.slice(2));

const DEFAULT_SETTINGS = {
  log: $.util.log,
  mode: '0755'
}

let connections = {}

gulp.task('upload', () => {
  let name = 'production'
  if (typeof argv.for === 'string') {
    name = argv.for
  }
  return upload(name)
})

export function upload (name) {
  let conn = getConnection(name)
  let { src, base, mode, remotePath } = conn.config

  return gulp
    .src(src, { base, buffer: false, cwd: ROOT_DIR })
    .pipe(conn.dest(remotePath || '/', { mode }))
}

export function rm(name, dir, cb) {
  let conn = getConnection(name)
  conn.rmdir(dir, cb)
}

function getConnection (name) {
  if (!connections[name]) {
    let settings = genarateSettings(ftpConfig[name])
    connections[name] = ftp.create(settings)
  }
  return connections[name]
}

function genarateSettings (settings) {
  if (!settings || !settings.host) {
    throw new Error(`Not found connection destination.`)
  }
  return extend(true, {}, DEFAULT_SETTINGS, settings)
}
