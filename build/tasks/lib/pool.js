import fs from 'fs'
import { getFormatter } from '../../../node_modules/eslint/lib/cli-engine'
import { logger, onClientConnected } from '../browser-sync'

let formatter = getFormatter('stylish')

let logs = {}

onClientConnected(() => {
  Object.keys(logs).forEach((type) => {
    logs[type] = logs[type].filter(({ filename }) => fs.statSync(filename))
  })

  Object.keys(logs).forEach((type) => {
    logs[type].forEach(({ message }, idx) => {
      logger(message, 'error')
    })
  })
})

export default function pool (type = '_default', result) {
  if (!logs[type]) {
    logs[type] = []
  }

  let _logs = logs[type]
  let message = null
  let filename = null

  if (type === 'browserify') {
    message = result.message
    filename = result.filename
  } else {
    let { messages, filePath } = result
    filename = filePath
    if (messages.length) {
      message = formatter([result])
    }
  }

  let idx = _logs.findIndex(log => log && log.filename === filename)

  if (message) {
    if (idx === -1) {
      _logs.push({ filename, message })
    } else {
      _logs[idx].message = message
    }
  } else {
    if (idx !== -1) {
      delete _logs[idx]
    }
  }
}
