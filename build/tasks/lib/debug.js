import minimist from 'minimist'

let $ = require('gulp-load-plugins')();

let argv = minimist(process.argv.slice(2));

export const IS_DEBUG = !!argv.debug

export default function debug (options) {
  return $.if(IS_DEBUG, $.debug(options))
}
