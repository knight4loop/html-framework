import gulp from 'gulp'
import fs from 'fs'
import path from 'path'
import sassdoc from 'sassdoc'
import through from 'through2'
import pool from './lib/pool'
import debug from './lib/debug'
import config, { SRC_DIR, DIST_DIR, ROOT_DIR } from '../.gulp-config'

let $ = require('gulp-load-plugins')()

gulp.task('compile:sass', () =>
  gulp.src(config.sass.src, { base: SRC_DIR, cwd: ROOT_DIR })
    .pipe(debug())
    .pipe($.plumber({
      errorHandler: $.notify.onError(error => {
        return {
          title: error.name,
          message: error.message
        }
      })
    }))
    .pipe($.cached('sass'))
    .pipe($.progeny({ rootPath: SRC_DIR }))
    .pipe($.sassLint({
      configFile: path.join(ROOT_DIR, './.sass-lint.yml')
    }))
    .pipe(through.obj(function (file, enc, cb) {
      if (file.isNull() || file.isStream()) {
        return cb();
      }
      file.sassLint.forEach((result) => {
        pool('sassLint', result)
      })
      this.push(file);
      cb();
    }))
    .pipe($.sassLint.format())
    .pipe($.sassLint.failOnError())
    .pipe($.sass(config.sass.options).on('error', $.sass.logError))
    .pipe(gulp.dest(DIST_DIR))
    .pipe(debug())
)

gulp.task('watch:sass', ['compile:sass'], () =>
  $.watch(config.sass.src, { cwd: ROOT_DIR }, () => gulp.start('compile:sass'))
)

gulp.task('docs:sass', () =>
  gulp.src(config.sass.src, { base: SRC_DIR, cwd: ROOT_DIR })
    .pipe(sassdoc(config.sassdoc))
)
