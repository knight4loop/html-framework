import gulp from 'gulp'
import path from 'path'
import minimist from 'minimist'
import requireDir from 'require-dir'
import runSequence from 'run-sequence'
import readlineSync from 'readline-sync'
import version from './version'
import { ROOT_DIR } from './.gulp-config'

let $ = require('gulp-load-plugins')()

let argv = minimist(process.argv.slice(2));

if (path.relative(process.cwd(), ROOT_DIR)) {
  process.chdir(ROOT_DIR)
}

requireDir('./tasks', { recursive: true });

gulp.task('default', $.taskListing.withFilters(/:/, /:/));

gulp.task('help', $.taskListing.withFilters(/:/));

gulp.task('clear-cache', () => $.cached.caches = {})

gulp.task('build', cb =>
  runSequence('clear-cache', 'delete', 'compile', cb)
);

gulp.task('compile', cb =>
  runSequence(
    'clear-cache',
    [
      'compile:ejs',
      'compile:sass',
      'compile:assets',
      'compile:ecmascript'
    ],
    cb
  )
);

gulp.task('run', cb =>
  runSequence(
    'build',
    'browser-sync',
    [
      'watch:ejs',
      'watch:sass',
      'watch:assets',
      'watch:ecmascript'
    ],
    cb
  )
);

gulp.task('deploy', cb => {
  if (argv.for === true || argv.for === 'production') {
    check('master')
  }
  return runSequence( 'build', 'upload', cb)
})

gulp.task('docs', cb =>
  runSequence(
    [
      'docs:sass',
      'docs:ecmascript'
    ],
    cb)
);

function check (allow) {
  if (!version.GIT_BRANCH) {
    return
  }
  if (version.GIT_BRANCH === allow) {
    return
  }
  if (!readlineSync.keyInYN(`Now is not on ${allow} branch, is on ${version.GIT_BRANCH} branch.\nAre you sure?\n-\n`)){
    $.util.log('Exit.')
    process.exit()
    return
  }
  $.util.log('Continue...')
}
