(function (socket) {

  socket.on('bs-msg:info', log => {
    console.error(log)
  })

  socket.on('bs-msg:warning', log => {
    console.warning(log)
  })

  socket.on('bs-msg:error', log => {
    console.error(log)
  })

})(window.___browserSync___.socket);
